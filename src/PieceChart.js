import Chart from "chart.js/auto";
import { Bar } from "react-chartjs-2";

function PieceChart({ pieces }) {
    const datasetLabels = { k: "King", q: "Queen", r: "Rook", b: "Bishop", n: "Knight", p: "Pawn" };
    const colorMap = {
        k: "#FD0100",
        q: "#F76915",
        b: "#EEDE04",
        n: "#A0D636",
        r: "#2fa236",
        p: "#333ED4",
    };

    const pieceCountTable = {
        k: [0, 0],
        q: [0, 0],
        b: [0, 0],
        n: [0, 0],
        r: [0, 0],
        p: [0, 0],
    };

    pieces.forEach(p => (pieceCountTable[p.type][p.color === "w" ? 0 : 1] += p.value));

    const datasets = [];
    for (let type in datasetLabels) {
        if (pieceCountTable[type].every(i => i === 0)) continue;

        datasets.push({
            label: datasetLabels[type],
            data: pieceCountTable[type],
            backgroundColor: colorMap[type],
        });
    }

    return (
        <div style={{ position: "relative", width: "25vmin" }}>
            <Bar
                data={{
                    labels: ["White", "Black"],
                    datasets,
                }}
                options={{
                    scales: {
                        x: {
                            stacked: true,
                        },
                        y: {
                            stacked: true,
                        },
                    },
                    aspectRatio: 0.25,
                }}
            />
        </div>
    );
}

export default PieceChart;
