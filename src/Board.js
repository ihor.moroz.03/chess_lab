import React from "react";
import "./Board.css";

class Board extends React.Component {
    static #letters = ["a", "b", "c", "d", "e", "f", "g", "h"];
    dragStartCoords;

    onDragStartHandler = coords => {
        this.dragStartCoords = coords;
    };

    onDropHandler = (ev, coords) => {
        ev.preventDefault();
        this.props.onDnd(this.dragStartCoords, coords);
    };

    indicesToCoordinates = (i, j) => ({ row: 8 - i, column: String.fromCharCode(j + 97) });

    render() {
        const pieces = [];
        for (let i = 0; i < 8; ++i) pieces[i] = new Array(8).fill(null);

        if (this.props.showCoordsGrid) {
            for (let i = 0; i < 8; ++i) {
                pieces[7][i] = [
                    <p className="column-letters" key={"coll" + i}>
                        {Board.#letters[i]}
                    </p>,
                ];
                pieces[i][0] ??= [];
                pieces[i][0].push(
                    <p className="row-numbers" key={"rown" + i}>
                        {8 - i}
                    </p>
                );
            }
        }

        this.props.pieces.forEach(({ row, column, color, type, id }) => {
            pieces[8 - row][column.charCodeAt(0) - 97] ??= [];
            pieces[8 - row][column.charCodeAt(0) - 97].push(
                <img
                    key={id}
                    src={require(`./assets/piece-images/${color + type}.png`)}
                    alt=""
                    onDragStart={() => this.onDragStartHandler({ row, column })}
                />
            );
        });

        return (
            <table id="board">
                <tbody>
                    {pieces.map((row, i) => (
                        <tr key={i + "tr"}>
                            {row.map((cell, j) => (
                                <td
                                    key={`${i}${j}td`}
                                    onDragOver={ev => ev.preventDefault()}
                                    onDrop={ev =>
                                        this.onDropHandler(ev, this.indicesToCoordinates(i, j))
                                    }
                                    onDoubleClick={() =>
                                        this.props.onDoubleClick(this.indicesToCoordinates(i, j))
                                    }
                                >
                                    {cell}
                                </td>
                            ))}
                        </tr>
                    ))}
                </tbody>
            </table>
        );
    }
}

export default Board;
