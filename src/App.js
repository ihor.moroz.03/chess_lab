import React from "react";
import "./App.css";
import Board from "./Board";
import PieceChart from "./PieceChart";
import BoardControls from "./BoardControls";
import Info from "./Info";
import RandomDog from "./RandomDog";

class App extends React.Component {
    state = {
        pieces: [],
        showCoordsGrid: true,
    };

    findPiece({ row, column }) {
        return this.state.pieces.find(p => p.row === row && p.column === column);
    }

    addPiece(piece, force) {
        let newPieces = this.state.pieces;
        const existingPiece = this.findPiece(piece);
        if (existingPiece != null) {
            force ??= window.confirm("Selected cell's occupied. Do you want to replace it?");
            if (!force) return;
            newPieces = newPieces.filter(p => p.id !== existingPiece.id);
        }
        newPieces.push(piece);
        this.setState({ pieces: newPieces });
    }

    removePiece({ row, column, id }) {
        id ??= this.findPiece({ row, column })?.id;
        const newPieces = this.state.pieces.filter(p => p.id !== id);
        this.setState({ pieces: newPieces });
    }

    onDndHandler(dragCoords, { row, column }) {
        const piece = this.findPiece(dragCoords);
        [piece.row, piece.column] = [row, column];
        this.addPiece(piece, true);
    }

    render = () => (
        <div className="App">
            <Board
                pieces={this.state.pieces}
                showCoordsGrid={this.state.showCoordsGrid}
                onDnd={this.onDndHandler}
                onDoubleClick={coords => this.removePiece(coords)}
            />
            <PieceChart pieces={this.state.pieces} />
            <BoardControls
                onShowCoordsToggle={() => {
                    const show = this.state.showCoordsGrid ? false : true;
                    this.setState({ showCoordsGrid: show });
                }}
                onAddPiece={piece => this.addPiece(piece)}
            />
            <Info />
            <RandomDog />
        </div>
    );
}

export default App;
