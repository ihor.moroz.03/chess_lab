import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "react-bootstrap/Button";
import Stack from "react-bootstrap/esm/Stack";

class Timer extends React.Component {
    state = { str: "00:00:00" };
    start = Date.now();
    savedValue = 0;
    interval;

    get value() {
        return Date.now() - this.start + this.savedValue;
    }

    updateString = () => {
        let seconds = this.value / 1000;
        let hours = parseInt(seconds / 3600);
        seconds -= hours * 3600;
        let minutes = parseInt(seconds / 60);
        seconds = parseInt(seconds - minutes * 60);

        const str = [hours, minutes, seconds]
            .map(val => (val < 10 ? `0${val}` : `${val}`))
            .join(":");
        this.setState({ str: str });
    };

    render() {
        return (
            <div>
                <h3>Timer</h3>
                <p style={{ fontSize: "7.5vmin" }}>{this.state.str}</p>
                <Stack direction="horizontal" gap={2}>
                    <Button
                        onClick={() => {
                            clearInterval(this.interval);
                            this.start = Date.now();
                            this.interval = setInterval(this.updateString, 1000);
                        }}
                    >
                        Start
                    </Button>
                    <Button
                        onClick={() => {
                            clearInterval(this.interval);
                            this.savedValue = this.value;
                        }}
                    >
                        Stop
                    </Button>
                    <Button
                        onClick={() => {
                            clearInterval(this.interval);
                            this.savedValue = 0;
                            this.setState({ str: "00:00:00" });
                        }}
                    >
                        Reset
                    </Button>
                </Stack>
            </div>
        );
    }
}

export default Timer;
