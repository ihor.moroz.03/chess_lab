import "bootstrap/dist/css/bootstrap.min.css";

function Info() {
    return (
        <div>
            <h2 className="my-3">Info</h2>
            <ul>
                <li>
                    <a
                        href="https://www.chess.com/puzzles/problem/491418"
                        rel="noreferrer"
                        target="_blank"
                    >
                        Puzzle
                    </a>
                </li>
                <li>
                    <a
                        href="https://www.chess.com/learn-how-to-play-chess"
                        rel="noreferrer"
                        target="_blank"
                    >
                        How to play chess
                    </a>
                </li>
            </ul>
        </div>
    );
}

export default Info;
