import React from "react";

import "bootstrap/dist/css/bootstrap.min.css";
import Button from "react-bootstrap/Button";

class RandomDog extends React.Component {
    state = {};

    updateDog = () => {
        // fetch("https://random.dog/woof.json?ref=apilist.fun")
        //     .then(res => res.json())
        //     .then(res => {
        //         this.setState({ resourseUrl: res.url });
        //     });
        const xhttp = new XMLHttpRequest();
        xhttp.onload = ({ target }) => {
            const url = JSON.parse(target.responseText).url;
            this.setState({ resourseUrl: url });
        };
        xhttp.open("GET", "https://random.dog/woof.json?ref=apilist.fun");
        xhttp.send();
    };

    componentDidMount() {
        this.updateDog();
    }

    render() {
        return (
            <div style={{ maxWidth: "100vmin" }}>
                <h2 className="my-3">Dog</h2>
                {this.state.resourseUrl?.match(/.mp4/g) ? (
                    <video width="100%" src={this.state.resourseUrl} autoPlay loop />
                ) : (
                    <img width="100%" src={this.state.resourseUrl} alt="dog" />
                )}
                <br />
                <Button className="mt-3" onClick={this.updateDog}>
                    Update
                </Button>
            </div>
        );
    }
}

export default RandomDog;
