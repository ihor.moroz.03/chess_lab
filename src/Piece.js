class Piece {
    static #values = { k: 0, q: 9, r: 5, b: 3, n: 3, p: 1 };

    constructor(row, column, color, type) {
        this.row = Number(row);
        this.column = column;
        this.color = color;
        this.type = type;

        this.id = Piece.#newId(this);
    }

    get value() {
        return Piece.#values[this.type];
    }

    static #newId(p) {
        return Math.random().toString(16).slice(2) + Date.now().toString().slice(-3);
    }
}

export default Piece;
