import React from "react";
import Piece from "./Piece";
import Timer from "./Timer";

import "bootstrap/dist/css/bootstrap.min.css";
import Form from "react-bootstrap/Form";
import Stack from "react-bootstrap/Stack";

class BoardControls extends React.Component {
    state = {
        color: "w",
        type: "k",
        row: "1",
        column: "a",
    };

    handleInputChange = (event, value) => {
        const target = event.target;
        value ??= target.type === "checkbox" ? target.checked : target.value;
        this.setState({ [target.name]: value });
    };

    render() {
        return (
            <div style={{ flexBasis: "300px", flexGrow: "1" }}>
                <h2 className="my-3">Board controls</h2>
                <Form
                    className="my-4"
                    onSubmit={ev => {
                        ev.preventDefault();
                        this.props.onAddPiece(
                            new Piece(
                                this.state.row,
                                this.state.column,
                                this.state.color,
                                this.state.type
                            )
                        );
                    }}
                >
                    <h3>Add piece</h3>
                    <Stack direction="horizontal" gap="3">
                        <Form.Check
                            label="White"
                            name="color"
                            type="radio"
                            id="radio-1"
                            defaultChecked
                            onChange={ev => this.handleInputChange(ev, "w")}
                        />
                        <Form.Check
                            label="Black"
                            name="color"
                            type="radio"
                            id="radio-2"
                            onChange={ev => this.handleInputChange(ev, "b")}
                        />
                    </Stack>
                    <Form.Select
                        className="mb-2"
                        name="type"
                        id="select-1"
                        onChange={this.handleInputChange}
                    >
                        <option value="k">King</option>
                        <option value="q">Queen</option>
                        <option value="b">Bishop</option>
                        <option value="n">Knight</option>
                        <option value="r">Rook</option>
                        <option value="p">Pawn</option>
                    </Form.Select>
                    <Stack direction="horizontal" gap="3">
                        <Form.Control
                            name="row"
                            type="number"
                            min={1}
                            max={8}
                            defaultValue={1}
                            onChange={this.handleInputChange}
                        />
                        <Form.Control
                            name="column"
                            type="text"
                            pattern="[a-h]{1}"
                            defaultValue="a"
                            placeholder="Column (a-h)"
                            onChange={this.handleInputChange}
                        />
                    </Stack>
                    <br />
                    <Form.Control type="submit" value="Add" />
                </Form>
                <hr />
                <Timer />
                <hr />
                <Form>
                    <Form.Check
                        label="Show coordinates"
                        type="switch"
                        id="switch-1"
                        defaultChecked
                        onChange={this.props.onShowCoordsToggle}
                    />
                </Form>
            </div>
        );
    }
}

export default BoardControls;
